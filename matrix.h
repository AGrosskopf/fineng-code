//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   matrix.h
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#ifndef matrixH
#define matrixH
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include <iostream>
#include <algorithm>
#include <vector>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   Class matrix                                                                              
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

class matrix
{
    public:
		matrix();                                          // default constructor
		matrix(long, long);                                // NxM constructor,  m(i,j) = 0
		matrix(long, long, double);                        // NxM constructor,  m(i,j) = a  
		matrix(long, long, const std::vector<double> &);   // NxM from a std::vector
		matrix(const std::vector<double> &);               // Nx1 from a std::vector
		matrix(const std::vector<std::vector<double> > &); // constructs from a v of vs

		operator std::vector<std::vector<double> >();      // constructs a v of vs from this
		
		//Rule of three stuff
		~matrix();                            // non-virtual,  so cannot be a base class
		matrix(const matrix &);
		matrix & operator=(const matrix &);
		 						
		void swap(matrix &);
		matrix * clone() const;
		
		//XXXXX indexing operators XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		const double & operator()(const long, const long) const; // Indexing (r-value)
		double & operator()(const long, const long);             // Indexing (l-value)
		const double & operator()(const long) const;
        double & operator()(const long);

		//XXXXX overloaded insertion operator XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		friend std::ostream & operator<<(std::ostream & out, const matrix & a);

		//XXXXX overloaded arithmetic operators XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		friend const matrix operator+(const matrix &, const matrix &); // elementwise
		friend const matrix operator-(const matrix &, const matrix &); // elementwise
		friend const matrix operator*(const matrix &, const matrix &); // matrix mult.
		friend const matrix operator/(const matrix &, const matrix &); // M*N^(-1)
		
		friend const matrix operator+(const matrix &, const double &); // element wise
		friend const matrix operator-(const matrix &, const double &);
		friend const matrix operator*(const matrix &, const double &);
		friend const matrix operator/(const matrix &, const double &);
		
		friend const matrix operator+(const double &, const matrix &); // element wise
		friend const matrix operator*(const double &, const matrix &);
		
		matrix & operator+=(const matrix &); // element wise
		matrix & operator-=(const matrix &); // element wise
		matrix & operator*=(const matrix &); // matrix mult.
		matrix & operator/=(const matrix &); // M*N^(-1)

		matrix & operator+=(const double &);  // member functions
		matrix & operator-=(const double &);
		matrix & operator*=(const double &);
		matrix & operator/=(const double &);

		friend const bool operator!=(const matrix &, const matrix &); // non-member
		friend const bool operator==(const matrix &, const matrix &); // non-member

        const matrix operator+() const {return *this;}
        const matrix operator-() const {return -1.0*(*this);}

		// matrix stuff		
		double trace() const;
		double det() const;
		long rows() const {return N_;}
		long cols() const {return M_;}

		matrix minor(long, long) const;  // creates a minor matrix
		
		const matrix transpose() const;  // member functions
		const matrix inverse() const;    // member functions
		
	private:
		double * x_;	// the data in row-major order
		long N_;	    // # rows	
		long M_;        // # cols	
		long S_;        // # elements = N_*M_	
        
        bool check_indexes(long, long) const;    // checks indexes
        bool check_size() const;                 // checks array bounds
        bool same_size(long, long) const;        // checks array bounds
        bool compatible_size(long) const;        // checks compatibility
        double * clone_data() const;       // Creates a new copy of x_
};

namespace std
{
	template<> void swap(matrix & a, matrix & b);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#endif
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
