//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  Input.h
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#ifndef InputH
#define InputH
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  class Input
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

class Input
{
    public:
        Input();
        
        double GetS0() const {return S_0_;}
        double GetR() const {return r_;}
        double GetSig() const {return sig_;}
        double GetMu() const {return mu_;}
        
        double GetV0() const {return v0_;}
        double GetLambda() const {return lambda_;}
        double GetVBar() const {return vbar_;}
        double GetEta() const {return eta_;}
        double GetRho() const {return rho_;}
        
        char GetPtype() const {return P_type_;}
        char GetCIRtype() const {return CIR_type_;}

        double GetX() const {return X_;}
        double GetT() const {return T_;}
        double GetdX() const {return dX_;}
        
        double GetExerciseInterval() const {return Ex_int_;}
        
        double GetdB() const {return dB_;}
        double GetB() const {return B0_;}
        double GetdT() const {return dT_;}
        
        char GetOtype() const {return O_type_;}

        unsigned long GetJ() const {return J_;}
        unsigned long GetN() const {return N_;}
        
        char GetAcctype() const {return Acc_type_;}
        char GetMethodType() const {return Meth_type_;}
        char GetValuationtype() const {return v_type_;}
                
    private:
        double S_0_;
        double r_;
        double sig_;
        double mu_;
        
        double v0_;
        double lambda_;
        double vbar_;
        double eta_;
        double rho_;
        
        char P_type_;
        char CIR_type_;
    
        double X_;
        double T_;
        double Ex_int_;
        
        double dX_;  // ratchet in X
        double dB_;  // ratchet in B
        double B0_;  // initial value of threshold
        double dT_;  // reset interval on ratchet times     
        
        char O_type_;
    
        unsigned long J_;     // number of sample paths
        unsigned long N_;     // number of time steps
        
        char Acc_type_;
        char Meth_type_;    
        char v_type_;    
};

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#endif
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  End
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
