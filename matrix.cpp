//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   matrix.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "matrix.h"
#include "utility.h"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <complex>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  namespace std
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

namespace std
{
	template<> void swap(matrix & q1, matrix & q2)
	{
		q1.swap(q2);
	}
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  Structural stuff N_ and M_ are 1-based
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

matrix::matrix()                                        // default constructor
: x_(0), N_(0), M_(0), S_(0)
{}

matrix::matrix(long N, long M)                          // NxM constructor,  m(i,j) = 0
: x_(0), N_(N), M_(M), S_(N_*M_)
{
	check_size();
    x_ = new double[S_];
	for(long i = 0; i != S_; ++i) x_[i] = 0.0;
}

matrix::matrix(long N, long M, double a)             // NxM constructor,  m(i,j) = a 
: x_(0), N_(N), M_(M), S_(N_*M_)
{
	check_size();
    x_ = new double[S_];
	for(long i = 0; i != S_; ++i) x_[i] = a;
} 

matrix::matrix(const std::vector<double> & v)      // Nx1 from a std::vector
: x_(0)
{
    N_ = v.size();
    M_ = 1;
    S_ = N_;

    x_ = new double[S_];
	for(long i = 0; i != S_; ++i) x_[i] = v[i];
}

matrix::matrix(long N, long M, const std::vector<double> & v) // NxM from a std::vector
: x_(0), N_(N), M_(M), S_(N_*M_)
{
	check_size();
		
	if (S_ != long(v.size())) throw std::runtime_error("matrix: wrong size initializer");

    x_ = new double[S_];
    
    for(long i = 0; i != S_; ++i) x_[i] = v[i];  
}
		
matrix::matrix(const std::vector<std::vector<double> >  & v) // constructs from a v of vs
: x_(0)
{
    N_ = v.size();
    M_ = v[0].size();
    S_ = N_*M_;
    
    x_ = new double[S_];    

	for(long i = 0; i != N_; ++i)
    {
        if(long(v[i].size()) != M_) 
        {
            delete [] x_;
            x_ = 0;
            throw std::runtime_error("matrix: bad input vector");
        }
        for(long j = 0; j != M_; ++j) x_[i*M_ + j] = v[i][j];
    }
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   member conversion operator
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

matrix::operator std::vector<std::vector<double> >()  // constructs a v of vs from this
{    
    std::vector<double> row(M_, 0.0);
    std::vector<std::vector<double> > R(N_, row);

	for(long i = 0; i != N_; ++i)
    {
        for(long j = 0; j != M_; ++j) R[i][j] = x_[i*M_ + j];
    }
    
    return R;
}	

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   rule of three
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

matrix::~matrix()
{
	delete [] x_;
}

matrix::matrix(const matrix & original)
:	x_(0)	// Initialized to 0 so can delete harmlessly
{
	try
	{
		if (original.x_ != 0) x_ = original.clone_data();
	}
	catch(...)
	{
		delete [] x_;
		x_ = 0;
		throw;
	}

	N_ = original.N_;
	M_ = original.M_;
	S_ = original.S_;
}

matrix & matrix::operator=(const matrix & original)
{
	matrix temp(original);    // exception safe idiom
	swap(temp);          
	return *this;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  swap() and clone()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void matrix::swap(matrix & other)
{
	std::swap(x_, other.x_);		
	std::swap(N_, other.N_);		
	std::swap(M_, other.M_);		
	std::swap(S_, other.S_);		
}

matrix * matrix::clone() const
{
	return new matrix(*this);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  minor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

matrix matrix::minor(long I, long J) const  // 1-based
{
    check_indexes(I, J);
    
    long N = N_ - 1;
    long M = M_ - 1;
    
    matrix m(N, M);
    
	for(long i = 0; i != N; ++i)
	{
        long ii = (i < I-1) ? i : i + 1;
        	    
	    for(long j = 0; j != M; ++j)
	    {
	        long jj = (j < J-1) ? j : j + 1;
            	    	    
	        m.x_[i*M + j] = x_[ii*M_ + jj];
    	}
	}
    return m;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   indexing.  1-based
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const double & matrix::operator()(const long i, const long j) const // Indexing (r-value)
{
    check_indexes(i, j);
    return x_[(i - 1)*M_ + j - 1];  
}

double & matrix::operator()(const long i, const long j)             // Indexing (l-value)
{
    check_indexes(i, j);
    return x_[(i - 1)*M_ + j - 1];  
}

const double & matrix::operator()(const long i) const               // Nx1 case
{
    return operator()(i, 1);  
}

double & matrix::operator()(const long i)             
{
    return operator()(i, 1); 
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  Overload <<
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

std::ostream & operator<<(std::ostream & out, const matrix & a)
{
    if(a.N_ == 0 || a.M_ == 0) return out;
    
	out << "( ";
	
	for(long i = 0; i != a.N_; ++i)
	{
	    out << a.x_[i*a.M_];
	    
	    for(long j = 1; j != a.M_; ++j)
	    {
    		out << ", " << a.x_[i*a.M_ + j]; 
    	}
	    if(i != a.N_ - 1) out << "  /  ";
	}
	
	return out << " )";
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   overloaded arithmetic operators                                                                              
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const matrix operator+(const matrix & m1, const matrix & m2)
{
    matrix temp(m1);
    temp += m2;
    return temp;    
}

const matrix operator-(const matrix & m1, const matrix & m2)
{
    matrix temp(m1);
    temp -= m2;
    return temp;    
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   matrix multiplication                                                                             
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const matrix operator*(const matrix & m1, const matrix & m2)
{
    matrix temp(m1);
    temp *= m2;
    return temp;    
}
	
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   m1*m2-1                                                                          
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const matrix operator/(const matrix & m1, const matrix & m2)
{
    matrix temp(m1);
    temp /= m2;
    return temp;    
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   element wise arithmetic overloads                                                                         
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const matrix operator+(const matrix & m , const double & s)
{
    matrix temp(m);
    temp += s;
    return temp;    
}
    
const matrix operator-(const matrix & m , const double & s)
{
    matrix temp(m);
    temp -= s;
    return temp;    
}
    
const matrix operator*(const matrix & m , const double & s)
{
    matrix temp(m);
    temp *= s;
    return temp;    
}
    
const matrix operator/(const matrix & m , const double & s)
{
    matrix temp(m);
    temp /= s;
    return temp;    
}
    		
const matrix operator+(const double & s, const matrix & m){return m + s;}
const matrix operator*(const double & s, const matrix & m){return m * s;}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   arithmetic assignment                                                                          
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

matrix & matrix::operator+=(const matrix & rhs)
{
	if ((N_ != rhs.N_) || (M_ != rhs.M_)) throw std::runtime_error("matrix: bounds mismatch");

	for(long i = 0; i != S_; ++i) x_[i] += rhs.x_[i]; // element wise
    return *this;
}

matrix & matrix::operator-=(const matrix & rhs)
{
	if ((N_ != rhs.N_) || (M_ != rhs.M_)) throw std::runtime_error("matrix: bounds mismatch");

	for(long i = 0; i != S_; ++i) x_[i] -= rhs.x_[i]; // element wise
    return *this;
}

matrix & matrix::operator*=(const matrix & rhs)  // matrix multiplication
{
    long m2_N = rhs.N_;
    long m2_M = rhs.M_;

	if (M_ != m2_N) throw std::runtime_error("matrix mult.: bounds mismatch");

	matrix result(N_, m2_M);
	
	for(long i = 0; i != N_; ++i)
	{
	    for(long j = 0; j != m2_M; ++j) 
        {
            long S = i*m2_M + j;
            result.x_[S] = 0.0;
            
            for(long k = 0; k != m2_N; ++k)
            {                
                result.x_[S] += x_[i*M_ + k] * rhs.x_[k*m2_M + j];
            }
        }
    }
	(*this) = result;     
    return *this;
}

matrix & matrix::operator/=(const matrix & rhs)
{
    (*this) *= rhs.inverse();  // multiplication by a matrix inverse
    return *this;
}
		
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   element wise scalar ops                                                                           
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

matrix & matrix::operator+=(const double & rhs)
{
	for(long i = 0; i != S_; ++i) x_[i] += rhs;
    return *this;
}

matrix & matrix::operator-=(const double & rhs)
{
	for(long i = 0; i != S_; ++i) x_[i] -= rhs;
    return *this;
}

matrix & matrix::operator*=(const double & rhs)
{
	for(long i = 0; i != S_; ++i) x_[i] *= rhs;
    return *this;
}

matrix & matrix::operator/=(const double & rhs)
{
	for(long i = 0; i != S_; ++i) x_[i] /= rhs;
    return *this;
}
		
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   matix stuff                                                                            
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

double matrix::trace() const
{
    long N = std::min(N_, M_);
    
    double tr = 0.0;
    for(long i = 0; i != N; ++i) tr += x_[i*N + i];
    
    return tr;
}

double matrix::det() const
{
    if(N_ != M_) throw std::runtime_error("matrix::det: matrix not square");
    
    if(1 == N_) return x_[0];
    
    if(2 == N_) return x_[0]*x_[3] - x_[1]*x_[2];
    
    if(3 == N_)  return x_[0]*(x_[4]*x_[8] - x_[5]*x_[7]) 
                          - x_[1]*(x_[3]*x_[8] - x_[5]*x_[6])
                          + x_[2]*(x_[3]*x_[7] - x_[4]*x_[6]);
               
   	long dt = 0.0;
   	long sgn = 1;
    for(long i = 0; i != N_; ++i)
   	{
   	    matrix m = this->minor(1, i+1);
   	    dt += sgn*x_[i]*m.det();
   	    sgn = -sgn;
   	}
    return dt;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   transpose()                                                                           
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const matrix matrix::transpose() const
{
    matrix m(M_, N_);

	for(long i = 0; i != M_; ++i)
	{	    
	    for(long j = 0; j != N_; ++j)
	    {          	    	    
	        m.x_[i*N_ + j] = x_[j*M_ + i];
    	}
	}	
    return m;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   inverse()                                                                           
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const matrix matrix::inverse() const
{
    if(N_ != M_) throw std::runtime_error("matrix::det: matrix not square");
    
    double dt = det();
    matrix m(N_, N_);
    if(2 == N_) 
    {
        m(1,1) = x_[3]/dt;   m(1,2) = -x_[1]/dt;
        m(2,1) = -x_[2]/dt;  m(2,2) = x_[0]/dt;
        return m;
    }
    
    long sgn_i = 1;
	for(long i = 0; i != N_; ++i)
	{	    
        long sgn_j = 1;
	    for(long j = 0; j != N_; ++j)
	    {          	    	    
	        m.x_[i*N_ + j] = sgn_i*sgn_j * minor(j+1, i+1).det()/dt;
	        sgn_j = -sgn_j;
    	}
    	sgn_i = -sgn_i;
	}
    return m;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   comparison operators                                                                            
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const bool operator!=(const matrix & m1, const matrix & m2)
{
	if ((m1.N_ != m2.N_) || (m1.M_ != m2.M_)) return true;

	long S = m2.S_;
	for(long i = 0; i != S; ++i) 
    {
        if(m1.x_[i] != m2.x_[i]) return true;
    }
	
	return false;
}

const bool operator==(const matrix & m1, const matrix & m2)
{
    return !(m1 != m2);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   helpers                                                                             
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

bool matrix::check_indexes(long i, long j) const    // checks indexes
{
    if(i < 1 || i > N_) throw std::runtime_error("matrix: i out of bounds");
    if(j < 1 || j > M_) throw std::runtime_error("matrix: j out of bounds");
    
    return true;
}

bool matrix::check_size() const    // checks array bounds
{
	if (N_ <= 0 || M_ <= 0) throw std::runtime_error("matrix::check_size: bad size");
    
    return true;
}

bool matrix::same_size(long N, long M) const // checks array bounds
{
    if (N_ != N || M_ != M) throw std::runtime_error("matrix::same_size: bad size");
    
    return true;
}

bool matrix::compatible_size(long N) const   // checks compatibility
{
    if (N != N_) throw std::runtime_error("matrix: cannot multiply");
    
    return true;
}

double * matrix::clone_data() const
{
	double * new_x = new double[S_];
	for(long i = 0; i != S_; ++i) new_x[i] = x_[i];
	return new_x;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   end of file                                                                              
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

