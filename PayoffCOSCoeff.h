//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  PayoffCOSCoeff.h
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#ifndef PayoffCOSCoeffH
#define PayoffCOSCoeffH
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "FunctionBase.h"

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  class PayoffCOSCoeff
//    Corresponds to eq 38 in Fang & Oosterlee (2011)
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

class PayoffCOSCoeff : public FunctionBase<unsigned long, double>
{
    public:
        PayoffCOSCoeff(char otype, double X, double a, 
                       double b, double l, double u);
        
        // Operator overloads
        double operator()(unsigned long k) const;
        
    private:
        // relevant varibles from PseudoFactory
        char otype_;
        double X_;
        
        // Non-subscript independent variables
        double a_, b_;
        double uStar_, lStar_;
        
        // Intermediate variables and common expressions
        double firstFourierPeriod_;
        double firstUPeriod_, firstLPeriod_;
        double exp_u_, exp_l_;
        double outerTerm_;
        
        // Chi_k and Phi_k functions
        double ComputeChi(unsigned long k) const;
        double ComputePhi(unsigned long k) const;
};

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#endif
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
