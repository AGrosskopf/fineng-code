//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	PayoffCOSCoeff.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "PayoffCOSCoeff.h"

#include <cmath>
#include <stdexcept>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	set the constant PI
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

namespace
{
    const double PI = 3.141592653589793;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	constructor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

PayoffCOSCoeff::PayoffCOSCoeff(char otype, double X, double a,
                               double b, double l, double u)
: otype_(otype), X_(X), a_(a), b_(b), uStar_(u), lStar_(l)
{ 
    // Determine alpha
    short alpha;
    switch(otype)
    {
        case 'c':  alpha = 1;          break;
        case 'p':  alpha = -1;         break;
        default:   throw std::runtime_error("PayoffCOSCoeff:  Bad option type");
    }
    
    // Determine lStar and uStar
    if (alpha == 1)
    {
        lStar_ = std::max(l, 0.0);
        uStar_ = std::max(u, 0.0);
    }
    else // alpha_ = -1
    {
        lStar_ = std::min(l, 0.0);
        uStar_ = std::min(u, 0.0);
    }
    
    // Compute the outer term
    outerTerm_ = 2.0*alpha*X_/(b - a);
    
    // Common expressions for both Chi and Phi
    firstFourierPeriod_ = PI/(b_ - a_);
    firstUPeriod_ = firstFourierPeriod_*(uStar_ - a);
    firstLPeriod_ = firstFourierPeriod_*(lStar_ - a);
    
    // Intermediate variables for Chi    
    exp_l_ = std::exp(lStar_);
    exp_u_ = std::exp(uStar_);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	ComputeChi()
//    Correspond to equation 42(a) in Fang & Oosterlee (2011)
//    Important note: k and n subscripts appear to be used interchangly in the 
//      above paper. Here we will only use k subscripts.
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

double PayoffCOSCoeff::ComputeChi(unsigned long k) const
{
    double fourierPeriod = firstFourierPeriod_*k; // k*PI/(b-a)
    double chiOuterTerm = 1.0/(1.0 + fourierPeriod*fourierPeriod);
    
    double uPeriod = k*firstUPeriod_; // k*PI*(u-a)/(b-a)
    double lPeriod = k*firstLPeriod_; // k*PI*(l-a)/(b-a)
    
    double cosTerms = cos(uPeriod)*exp_u_ - std::cos(lPeriod)*exp_l_;
    double sinTerms = fourierPeriod*std::sin(uPeriod)*exp_u_ 
                       - fourierPeriod*std::sin(lPeriod)*exp_l_;
    double innerTerms = cosTerms + sinTerms;
    
    return chiOuterTerm*innerTerms;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	ComputePhi()
//    Correspond to equation 42(b) in Fang & Oosterlee (2011)
//    Important note: k and n subscripts appear to be used interchangly in the 
//      above paper. Here we will only use k subscripts.
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

double PayoffCOSCoeff::ComputePhi(unsigned long k) const
{
    if (k == 0)     return uStar_ - lStar_;
    
    double uPeriod = k * firstUPeriod_; // k*PI*(u-a)/(b-a)
    double lPeriod = k * firstLPeriod_; // k*PI*(l-a)/(b-a)
    double inv_subterm1 = 1.0/(k*firstFourierPeriod_);
    
    double phi_k = (sin(uPeriod) - sin(lPeriod))*inv_subterm1;     
    return phi_k;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	operator()
//    Correspond to equation 38 in Fang & Oosterlee (2011)
//    Important note: k and n subscripts appear to be used interchangly in the 
//      above paper. Here we will only use k subscripts.
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

double PayoffCOSCoeff::operator()(unsigned long k) const
{
    return outerTerm_ * (ComputeChi(k) - ComputePhi(k));
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

