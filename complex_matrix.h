//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   complex_matrix.h
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#ifndef complex_matrixH
#define complex_matrixH
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "matrix.h"

#include <iostream>
#include <algorithm>
#include <vector>
#include <complex>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   class complex_matrix                                                                              
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

class complex_matrix
{
    public:
        // default constructor
		complex_matrix();                                          
		// NxM constructor,  m(i,j) = 0
		complex_matrix(long, long);                                
		// NxM constructor,  m(i,j) = a
		complex_matrix(long, long, std::complex<double>);                          
		// NxM from a std::vector
		complex_matrix(long, long, const std::vector<std::complex<double>> &);   
		// Nx1 from a std::vector
		complex_matrix(const std::vector<std::complex<double>> &);               
		// constructs from a v of vs
		complex_matrix(const std::vector<std::vector<std::complex<double>> > &); 
		// cast a matrix to a complex matrix
	    complex_matrix(const matrix &);											 
        
        // constructs a v of vs from this
		operator std::vector<std::vector<std::complex<double>> >();      
		// constructs matrix from real component
		matrix real() const;					     		 				 	
		
		//Rule of three stuff
		~complex_matrix();    // non-virtual,  so cannot be a base class
		complex_matrix(const complex_matrix &);
		complex_matrix & operator=(const complex_matrix &);
		 						
		void swap(complex_matrix &);
		complex_matrix * clone() const;
		
		//XXXXX indexing operators XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  
        // Indexing (r-value)
		const std::complex<double> & operator()(const long, const long) const; 
		// Indexing (l-value)
		std::complex<double> & operator()(const long, const long);             
		const std::complex<double> & operator()(const long) const;
        std::complex<double> & operator()(const long);

		//XXXXX overloaded insertion operator XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		friend std::ostream & operator<<(std::ostream & out, 
                                         const complex_matrix & a);

		//XXXXX overloaded arithmetic operators XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

        // elementwise
		friend const complex_matrix operator+(const complex_matrix &, 
                                              const complex_matrix &); 
		// elementwise
		friend const complex_matrix operator-(const complex_matrix &, 
                                              const complex_matrix &); 
		// complex_matrix mult.
		friend const complex_matrix operator*(const complex_matrix &, 
                                              const complex_matrix &); 
		// M*N^(-1)
		friend const complex_matrix operator/(const complex_matrix &, 
                                              const complex_matrix &); 
		
		// element wise
		friend const complex_matrix operator+(const complex_matrix &, 
                                              const std::complex<double> &); 
		friend const complex_matrix operator-(const complex_matrix &, 
                                              const std::complex<double> &);
		friend const complex_matrix operator*(const complex_matrix &, 
                                              const std::complex<double> &);
		friend const complex_matrix operator/(const complex_matrix &, 
                                              const std::complex<double> &);
		
		// element wise
		friend const complex_matrix operator+(const std::complex<double> &, 
                                              const complex_matrix &); 
		friend const complex_matrix operator*(const std::complex<double> &, 
                                              const complex_matrix &);

        // Element-wise multiplication
		const complex_matrix hadamard(const matrix &);
        const complex_matrix hadamard(const complex_matrix &);
        
        // element wise
        complex_matrix & operator+=(const complex_matrix &); 
        // element wise
		complex_matrix & operator-=(const complex_matrix &); 
		// complex_matrix mult.
		complex_matrix & operator*=(const complex_matrix &); 
		// M*N^(-1)
		complex_matrix & operator/=(const complex_matrix &); 

        // member functions
		complex_matrix & operator+=(const std::complex<double> &);  
		complex_matrix & operator-=(const std::complex<double> &);
		complex_matrix & operator*=(const std::complex<double> &);
		complex_matrix & operator/=(const std::complex<double> &);
        
        // non-member
		friend const bool operator!=(const complex_matrix &, 
                                     const complex_matrix &); 
		// non-member
		friend const bool operator==(const complex_matrix &, 
                                     const complex_matrix &); 

        const complex_matrix operator+() const {return *this;}
        const complex_matrix operator-() const {return -1.0*(*this);}

		// complex_matrix stuff		
		std::complex<double> trace() const;
		std::complex<double> det() const;
		long rows() const {return N_;}
		long cols() const {return M_;}

        // creates a minor complex_matrix
		complex_matrix minor(long, long) const;  
		
		const complex_matrix transpose() const;   // member functions
		const complex_matrix inverse() const;     // member functions
		
	private:
		std::complex<double> * x_;	              // the data in row-major order
		long N_;	                              // # rows	
		long M_;                                  // # cols	
		long S_;                                  // # elements = N_*M_	
        
        bool check_indexes(long, long) const;     // checks indexes
        bool check_size() const;                  // checks array bounds
        bool same_size(long, long) const;         // checks array bounds
        bool compatible_size(long) const;         // checks compatibility
        std::complex<double> * clone_data() const;// Creates a new copy of x_
};

namespace std
{
	template<> void swap(complex_matrix & a, complex_matrix & b);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#endif
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
