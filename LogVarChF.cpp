//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	LogVarChF.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "LogVarChF.h"
#include "ModBesselIv.h"

#include <iostream>
#include <cmath>
#include <stdexcept>
#include <complex>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	constructor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

LogVarChF::LogVarChF(double lambda, double vbar, double eta, 
                     double tau, double v_s, double v_t)
: lambda_(lambda), vbar_(vbar), eta_(eta), tau_(tau), v_s_(v_s), v_t_(v_t)
{
    // Check validity of time parameters
    if (tau <= 0)
    {
        throw std::runtime_error("LogVarChF::Constructor:  Invalid times");
    }
       
    // Compute static terms   
    etaSqr_ = eta_ * eta_;
    q_ = 2.0 * lambda_ * vbar_ / etaSqr_ - 1.0;
    
    //Set sensible value
    SetTimes(tau_, v_s_, v_t_);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	SetTimes()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void LogVarChF::SetTimes(double tau, double v_s, double v_t)
{
    if (tau <= 0)
    {
        throw std::runtime_error("LogVarChF::SetTimes:  Invalid times");
    }
    
    tau_ = tau;
    v_s_ = v_s;
    v_t_ = v_t;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	LogVarChF::operator()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

std::complex<double> LogVarChF::operator()(std::complex<double> v) const
{
    // If v = 0, then ChF = E[1] = 1
    if (v == std::complex<double> (0.0, 0.0))
    {
        return std::complex<double> (1.0, 0.0);
    }

    // Preparation and aliasing
    std::complex<double> i(0.0, 1.0);
    std::complex<double> gamma_v = std::sqrt(lambda_*lambda_ - 2.0*i*etaSqr_*v);
    
    //find common terms
    double SqrtVsVt = std::sqrt(v_s_*v_t_);
    
    //find large terms
    std::complex<double> exp_haf_NegvGamma = std::exp(-0.5*gamma_v*tau_);
    std::complex<double> One_min_expGammatau = 1.0 - std::exp(-gamma_v*tau_);
    std::complex<double> One_plus_expGammatau = 1.0 + std::exp(-gamma_v*tau_);
    
    double exp_haf_NegvLamda = std::exp(-0.5*lambda_*tau_);
    double One_min_expLamdatau = 1.0 - std::exp(-lambda_*tau_);
    double One_plus_expLamdatau = 1.0 + std::exp(-lambda_*tau_);
    
    std::complex<double> exp_GamLamtau =std::exp(-0.5*(gamma_v - lambda_)*tau_);
    
    //find inner of bessel
    std::complex<double> sub_bes_nume = SqrtVsVt*(4.0*gamma_v*exp_haf_NegvGamma) 
                                        / (etaSqr_*One_min_expGammatau);
    std::complex<double> sub_bes_domi = SqrtVsVt*(4.0*lambda_*exp_haf_NegvLamda) 
                                        / (etaSqr_*One_min_expLamdatau);

    // Complex bessel can be large, using long double until division
    // division should ensure within range of double
    ModBesselIv I_q(q_);
    std::complex<long double> besselFracL =I_q(sub_bes_nume) /I_q(sub_bes_domi);
    std::complex<double> besselFrac((double) besselFracL.real(),
                                    (double) besselFracL.imag());
    
    //find LogVarChF
    std::complex<double> ChF;
    ChF = besselFrac
          * (gamma_v*exp_GamLamtau*One_min_expLamdatau 
             / (lambda_*One_min_expGammatau))                   
          * std::exp( (v_s_ + v_t_)/etaSqr_
                      * ( (lambda_*One_plus_expLamdatau/One_min_expLamdatau)
                        - (gamma_v*One_plus_expGammatau/One_min_expGammatau))
                    );
    
    return ChF;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
