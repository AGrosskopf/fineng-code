//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	FangOosterlee.h
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#ifndef FangOosterleeH
#define FangOosterleeH
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "MethodBase.h"
#include "PseudoFactory.h"
#include "matrix.h"
#include "complex_matrix.h"
#include "OptionBermudanBase.h"

#include <vector>
#include <map>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  class FangOosterlee
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

class FangOosterlee : public MethodBase
{
    public:
        // Typedefs
        typedef std::vector<complex_matrix> cMatrixVec;
        
        FangOosterlee(const PseudoFactory & fac);
            
        void run(const OptionBase & opt, const ProcessBase & proc);
        void run(const OptionBermudanBase & opt, const ProcessBase & proc);
        
    private:      
        // A copy of the relevant variables
        double s0_;         double X_;
        double r_;          double tau_;
        char otype_;        double v0_;
        double lambda_;     double vbar_;
        double T_;          double eta_;
        double mu_;         double rho_;
        double etaSqr_;
    
        // Truncation range
        double av_, bv_;   // log-var transition PDF significantly > 0
        double a_, b_;     // log-stock conditional PDF sigficantly > 0

        // fourier series terms
        unsigned long N_;
        
        // Quadrature stuff
        unsigned long J_;  // number of nodes
        matrix quadNodes_; // nodes
        matrix w_;         // weights
        
        // Phi_j matrices
        cMatrixVec phi_j_;

        // Final option value
        double optionValue_;
        
        // Preparation phase operations
        void DetermineTruncationRanges(const ProcessBase & proc);
        void SetupQuadrature();
        const matrix ComputeVtM();
        const complex_matrix ComputePhi(unsigned long j);

        // Iteration phase operations
        double DetermineEarlyExercise(const OptionBermudanBase & opt, 
                                      unsigned long p, const matrix & vt);
        const complex_matrix GetMsMatrix(double l, double u);
        const complex_matrix GetMcMatrix(double l, double u);
        const matrix GetGMatrix(double l, double u);

        // MethodBase implementations
        void RegisterOutput(Output & out);
        double GetOptionValue() const { return optionValue_; }
};
 
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#endif
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
