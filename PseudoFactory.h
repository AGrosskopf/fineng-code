//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  PseudoFactory.h
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#ifndef PseudoFactoryH
#define PseudoFactoryH
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

class OptionBase;
class ProcessBase;
class MethodBase;
class ApplicationBase;

class Input;
class Output;

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  class PseudoFactory
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

class PseudoFactory
{
    public:
        double GetS0() const;
        double GetR() const;
        double GetSig() const;
        double GetMu() const;

        double GetV0() const;
        double GetLambda() const;
        double GetVBar() const;
        double GetEta() const;
        double GetRho() const;
        
        char GetPtype() const;

        double GetX() const;
        double GetT() const;
        
        double GetExerciseInterval() const;
        
        double GetdX() const;
        double GetdB() const;
        double GetB() const;
        double GetdT() const;

        char GetOtype() const;

        unsigned long GetJ() const;
        unsigned long GetN() const;
        
        void SetInput(Input * inp){inp_ = inp;}
        void SetOutput(Output * out){out_ = out;}
        
        Output * GetOutput() const {return out_;}
       
        OptionBase * CreateOption();
        ProcessBase * CreateProcess();
        MethodBase * CreateMethod();
        ApplicationBase * CreateApplication();
                
    private:
        Input * inp_;
        Output * out_;
};

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#endif
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  End
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
