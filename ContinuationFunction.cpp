//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  ContinuationFunction.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "ContinuationFunction.h"

#include <stdexcept>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	set the constant complex i and PI
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

namespace
{
    // Complex stuff
    typedef std::complex<double> complexDouble;
    complexDouble i(0.0, 1.0);

    // Constants
    constexpr double PI = 3.14159265358979323846;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  constructor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

ContinuationFunction::ContinuationFunction(double r, double tau, double a, 
                                           double b, unsigned long p, matrix w,
                                           matrix V, cMatrixVec phiMatrices)
: r_(r), tau_(tau), a_(a), b_(b), p_(p), w_(w), V_(V), phiMatrices_(phiMatrices)
{
    //define N rows and J columns
    N_ = V_.rows();
    J_ = V_.cols();

    //common expressions
    exp_rtau_ = std::exp(-r_*tau_);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  operator(x_m) - evaluate function at x_m with other vars held constant
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

double ContinuationFunction::operator()(const double x_m) const
{
    std::complex<double> compPi_frac = i*PI*(x_m - a_)/(b_ - a_);
    std::complex<double> currentTerm;
    
    //set summation variable
    std::complex<double> sum(0.0, 0.0);
    
    for (long n = 0; n < N_; ++n)
    {
        //change type of n
        double dn = (double) n;

        // Evaluate current sum term
        currentTerm = beta(n)*std::exp(dn*compPi_frac);

        // Sum up, for sigma dash first term is multiplied by 0.5
        if (n == 0)     currentTerm *= 0.5;
        sum += currentTerm;
    }
    
    //take real part
    double sumReal = std::real(sum);
    
    //c3 value
    double c3 = exp_rtau_*sumReal;
    return c3;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  operator(n, x_m) - evaluate nth derivate at x_m with other vars constant
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

double ContinuationFunction::operator()(const unsigned long derivativeOrder,
                                        const double x_m) const
{
    // Check derivative order
    if (derivativeOrder == 0)     return this->operator()(x_m);
    else if (derivativeOrder > 1)
    {
        // Throw error (post-C89 compiler concatination invoked)
        std::string msg = "ContinuationFunction::operator()"
                "Only 1st derivative supported";
        throw std::invalid_argument(msg);
    }

    std::complex<double> iPiFrac = i*PI*(x_m - a_)/(b_ - a_);
    std::complex<double> iPiFracDerivative = i*PI/(b_ - a_);
    std::complex<double> currentTerm;

    //set summation variable
    std::complex<double> sum(0.0, 0.0);

    for (long n = 0; n < N_; ++n)
    {
        //change type of n
        double dn = (double) n;

        // Evaluate current sum term
        currentTerm = beta(n)*std::exp(dn*iPiFrac)
                    * dn*iPiFracDerivative; // differentiated terms

        // Sum up, for sigma dash first term is multiplied by 0.5
        if (n == 0)     currentTerm *= 0.5;
        sum += currentTerm;
    }

    //take real part
    double sumReal = std::real(sum);

    //c3 value
    double c3 = exp_rtau_*sumReal;
    return c3;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  beta()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

std::complex<double> ContinuationFunction::beta(unsigned long n) const
{
    //declare variables
    std::complex<double> beta_n;
    std::complex<double> sum_beta_n(0.0, 0.0);

    // n+1 where one-based matrices are concerned
    unsigned long oneBase_n = n + 1;
    
    //summation for j = 0 to J_ - 1
    for (long j = 1; j != J_ + 1; ++j)
    {
        const complex_matrix & phi_j = phiMatrices_[j];
        double Vnj = V_(oneBase_n, j);
        std::complex<double> phi_jnp = phi_j(oneBase_n, p_);
        beta_n = w_(j) * Vnj * phi_jnp;
        sum_beta_n += beta_n;
    }
    
    return sum_beta_n;
}
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
