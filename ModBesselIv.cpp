//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	ModBesselIv.cpp
//  Credit: Some code here is derived from the book Numerical Recipes.
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "rv_library.h"
#include "ModBesselIv.h"

#include <limits>
#include <cmath>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	File level aliases and constants
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

namespace
{
    // Factorial stuff
    static const unsigned long facTerms = 10;
    static const double fac[facTerms] = {1, 1, 2, 6, 24, 120, 720, 5040, 40320,
                                         362880};
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	constructor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

ModBesselIv::ModBesselIv(double v)
: v_(v)
{
    // nothing else to do
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	ModBesselIv operator(complex)
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

ModBesselIv::complexLDouble ModBesselIv::operator()(complexLDouble z) const
{

    complexLDouble I_vX;
    long double ldHalf  = 0.5L;
    for(unsigned long i = 0; i < facTerms; ++i)
    {
        complexLDouble base = z*ldHalf;
        complexLDouble power = 2.0L*i + v_;
        complexLDouble numerator = std::pow(base, power);
        complexLDouble denominator = (fac[i]*std::tgamma(i + v_ + 1));
        I_vX += numerator/denominator;
    }
    return I_vX;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	ModBesselIv operator(double)
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

ModBesselIv::lDouble ModBesselIv::operator()(lDouble x) const
{
    // Library bessel only valid for positive order v
    // For v < 0: Use I(v,x) = (2(v+1)/x)*I(v+1,x) + I(v+2,x) recursively
    if (v_ < 0)
    {
        // Recursive call
        ModBesselIv bessVP1(v_ + 1.0);
        ModBesselIv bessVP2(v_ + 2.0);
        lDouble io = 2.0*(v_ + 1)*bessVP1(x)/x + bessVP2(x);
        return io;
    }

    // Base case
    double io, ko, ipo, kpo;
    RandomVariableStatisticalFunctions::bessik(x, v_, io, ko, ipo, kpo);
    return io;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
