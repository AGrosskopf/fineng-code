//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	ProcessHeston.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "ProcessHeston.h"

#include <iostream>
#include <cmath>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	constructor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

ProcessHeston::ProcessHeston(const PseudoFactory & params)
: lambda_(params.GetLambda()), T_(params.GetT()), vBar_(params.GetVBar()),
  v0_(params.GetV0()), mu_(params.GetMu()), eta_(params.GetEta()),
  rho_(params.GetRho())
{}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	GetCumulants()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

std::map<unsigned long, double> ProcessHeston::GetCumulants() const
{
    std::map<unsigned long, double> cumulants;
    cumulants[1] = GetFirstCumulant();
    cumulants[2] = GetSecondCumulant();
    return cumulants;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	GetFirstCumulant()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

double ProcessHeston::GetFirstCumulant() const
{
    double exp_lamdaT  = std::exp(-lambda_*T_);
    double fract_dV_2lamd = (vBar_ - v0_) / (2.0*lambda_);
    double multp_MuT   = mu_*T_;
    double multp_VbarT = vBar_*T_;
    
    return multp_MuT + ((1.0 - exp_lamdaT)*fract_dV_2lamd) - (0.5*multp_VbarT);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	GetSecondCumulant()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

double ProcessHeston::GetSecondCumulant() const
{
    // Find common terms
    double exp_lambdaT = std::exp(-lambda_*T_);
    double V0mVBar = v0_ - vBar_;
    double rhoEta = rho_*eta_;
    double etaSqr = eta_*eta_;
    double lambdaSqr = lambda_*lambda_;
    
    // Find the outer term
    double lambdaCubed = lambdaSqr * lambda_;
    double outer = 1.0 / (8.0 * lambdaCubed);
    
    // Now the inner term, trying to keep true to the rendering in Fang (2008)
    double inner;
    inner = eta_*T_*lambda_*exp_lambdaT*V0mVBar*(8.0*lambda_*rho_ - 4.0*eta_)
          + lambda_*rhoEta*(1.0 - exp_lambdaT)*(16.0*vBar_ - 8.0*v0_)
          + 2.0*vBar_*lambda_*T_*(-4.0*lambda_*rhoEta + etaSqr + 4.0*lambdaSqr)
          + etaSqr*( (vBar_- 2.0*v0_)*exp_lambdaT*exp_lambdaT
                   + vBar_*(6.0*exp_lambdaT - 7.0) + 2.0*v0_)
          + 8.0*lambdaSqr*V0mVBar*(1.0 - exp_lambdaT);
    
    // Return the product
    return outer*inner;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
