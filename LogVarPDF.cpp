//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	LogVarPDF.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "LogVarPDF.h"
#include "ModBesselIv.h"

#include <iostream>
#include <cmath>
#include <stdexcept>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	constructor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

LogVarPDF::LogVarPDF(double lambda, double T, double v0, double eta,double vbar)
: lambda_(lambda), tau_(T),
  sigma_s_(std::log(v0)),
  etaSqr_(eta * eta),
  q_(2.0 * lambda * vbar / etaSqr_ - 1.0)
{
    // Set sensible value for each common expression
    SetTimes(tau_, sigma_s_);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	SetTimes()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void LogVarPDF::SetTimes(double tau, double sigma_s)
{
    if (tau <= 0)
    {
        throw std::runtime_error("LogVarPDF::SetTimes:  Invalid times");
    }
    
    // Update time-dependent variables
    tau_ = tau;
    sigma_s_= sigma_s;
    
    // Update common terms
    exp_lambdaTau_ = std::exp(-lambda_ *tau );
    zeta_ = 2.0 * lambda_ / ((1.0 - exp_lambdaTau_ ) * etaSqr_);
    u_ = zeta_ * std::exp(sigma_s_ - lambda_ * tau);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	LogVarPDF::operator() return density value
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

double LogVarPDF::operator()(const double sigma_t) const
{
    //Compute newly determined common expressions
    double exp_sigt = std::exp(sigma_t);
    double exp_sigs = std::exp(sigma_s_);
    double exp_lambdaTauhalf = std::exp(-0.5*lambda_*tau_);
    double exp_neg_zeta = std::exp(-zeta_*(exp_sigs*exp_lambdaTau_ + exp_sigt));
    double exp_frac = std::pow(exp_sigt/(exp_sigs * exp_lambdaTau_), q_/2.0);

    // Get bessel functions
    double besselInput = 2.0*zeta_*exp_lambdaTauhalf
                         * std::sqrt(exp_sigt*exp_sigs);
    ModBesselIv I_q(q_);

    double besselOut = I_q(besselInput);
    double expTerms  = exp_neg_zeta*exp_frac*exp_sigt;
    double density   = zeta_*expTerms*besselOut;
    return density;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	LogVarPDF::operator() return function value
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

double LogVarPDF::operator()(const unsigned long n, const double sigma_t) const
{
    // Check derivative order
    if (n == 0)     return this->operator()(sigma_t);
    else if (n > 1)
    {
        // Throw error
        std::string msg = "LogVarPDF::operator() Only 1st derivative supported";
        throw std::invalid_argument(msg);
    }

    // Compute newly determined common expressions
    double exp_sigt = std::exp(sigma_t);
    double exp_u_zeta = std::exp(-u_ - zeta_*exp_sigt + sigma_t);
    double pow_haf_q = std::pow(zeta_*exp_sigt/u_, q_/2.0);

    // Get bessel functions
    double besselInput = 2.0*std::sqrt(zeta_*exp_sigt*u_);
    ModBesselIv I_q(q_);
    ModBesselIv I_qP1(q_ + 1.0);
    long double I_qOut = I_q(besselInput);
    long double I_qP1Out = I_qP1(besselInput);

    // Return density, the bracket is intentionally
    // multiplied out to maximise accuracy
    double density = (double) -(zeta_*exp(sigma_t)*I_qOut-q_*I_qOut
                                - I_qP1Out*zeta_*exp(sigma_t) -I_qOut)
                              * zeta_ * exp_u_zeta * pow_haf_q;
    return density;
}
 
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
