//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  ContinuationFunction.h
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#ifndef ContinuationFunctionH
#define ContinuationFunctionH
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "DifferentiableFunctionBase.h"
#include "matrix.h"
#include "complex_matrix.h"

#include <complex>
#include <vector>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  class ContinuationFunction
//  This class corresponds to the equation 32 and 33 of Fang and Oosterlee
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

class ContinuationFunction : public DifferentiableFunctionBase<double, double>
{
    public:
        // typedefs
        typedef const std::vector<complex_matrix> & cMatrixVec;
        ContinuationFunction(double r, double tau, double a, double b,
                             unsigned long p, matrix w, matrix V,
                             cMatrixVec phiMatrices);
        ~ContinuationFunction(){};
        
        // eq. 32
        double operator()(const double x_m) const; 
        // eq. 32 derivatives
        double operator()(unsigned long n, double x_m) const;
         
        // eq. 33
        std::complex<double> beta(unsigned long n) const; 
        
    private:
        // relevant valiables
        double r_;
        double tau_;
        double a_, b_;
        
        //calling column p
        unsigned long p_;
    
        // common expressions
        double exp_rtau_;
        
        //relevant matrices
        matrix w_;
        matrix V_;
        cMatrixVec phiMatrices_;
        
        //matrices rows and cols
        long N_;
        long J_;
};

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#endif
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
