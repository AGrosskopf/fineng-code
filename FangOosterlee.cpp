//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	FangOosterlee.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "FangOosterlee.h"
#include "ContinuationFunction.h"
#include "LogVarConditionalChF.h"
#include "LogVarPDF.h"
#include "M_J.h"
#include "NewtonMethod.h"
#include "ProcessBase.h"
#include "PayoffCOSCoeff.h"
#include "OptionBermudanCall.h"
#include "OptionBermudanPut.h"
#include "LinearComposition.h"
#include "Recursion_FFT.h"
#include "SplineInterpolation.h"
#include "Output.h"

#include <iostream>
#include <cmath>
#include <complex>
#include <vector>
#include <map>
#include <typeinfo>
#include <utility>
#include <memory>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	set the constants tolerance, complex i and PI
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

namespace
{
    const double TOL = 1e-7;
    std::complex<double> i(0.0, 1.0);
    constexpr double PI = 3.14159265358979323846;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	constructor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

FangOosterlee::FangOosterlee(const PseudoFactory & fac)
: s0_(fac.GetS0()), X_(fac.GetX()), r_(fac.GetR()),
  tau_(fac.GetExerciseInterval()), otype_(fac.GetOtype()), v0_(fac.GetV0()),
  lambda_(fac.GetLambda()), vbar_(fac.GetVBar()), T_(fac.GetT()),
  eta_(fac.GetEta()), mu_(fac.GetMu()), rho_(fac.GetRho()), 
  etaSqr_(eta_*eta_), N_(fac.GetN()), J_(fac.GetJ()),
  quadNodes_(fac.GetJ(), 1), w_(fac.GetJ(), 1), phi_j_(J_+ 1)
{
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Run() - General option
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void FangOosterlee::run(const OptionBase & opt, const ProcessBase & proc)

{
    // Check type of bermudan
    if (typeid(opt) == typeid(OptionBermudanCall))
    {
        // Cast to call
        OptionBermudanCall bermudanCall = (OptionBermudanCall) opt;
        run(bermudanCall, proc);
    }
    else if (typeid(opt) == typeid(OptionBermudanPut))
    {
        // Cast to put
        OptionBermudanPut bermudanPut = (OptionBermudanPut) opt;
        run(bermudanPut, proc);
    }
    else
    {
        throw std::runtime_error("FangOosterlee::run:  Unsupported option");
    }
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Run() - Bermudan option
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void FangOosterlee::run(const OptionBermudanBase & opt,const ProcessBase & proc)
{
    // Find a, b, a_v and b_v
    DetermineTruncationRanges(proc);
    SetupQuadrature();

    // Get matrix v(t_M) (at time M)
    matrix vtM = ComputeVtM();
    
    // Will store J 1-based continuation values, 1 per log-var grid point
    std::vector<double> contValM(J_ + 1);

    // Prepare matrix phi(j) for j = 1, ..., J
    for (unsigned long j = 1; j != J_ + 1; ++j)
    {
        complex_matrix phi_j = ComputePhi(j);
        phi_j_[j] = phi_j;
    }

    // Get early exercise count
    long M = opt.GetEarlyExerciseCount();
    double x0 = std::log(s0_/X_);

    // for each point on the log-var grid
    for (unsigned long p = 1; p != J_ + 1; ++p)
    {
        // Record the inital vt
        matrix currentVt = vtM;

        // For each timestep
        for (long m = M; m > 0; --m)
        {
            // Determine early-exercise point
            double xStar = DetermineEarlyExercise(opt, p, currentVt);

            // Get M = Ms + Mc, first we need to determine l and u
            double l, u;
            switch(otype_)
            {
                case 'c':  l = xStar; u = b_;   break;
                case 'p':  l = a_; u = xStar;   break;
                default:   
                    throw std::runtime_error("FangOosterlee:  Bad option type");
            }
            complex_matrix matrixM = GetMsMatrix(l, u) + GetMcMatrix(l, u);

            // Compute B-hat column by column
            complex_matrix priorB_hat(N_, J_);
            for (unsigned j = 1; j != J_ + 1; ++j)
            {
                // Compute beta-hat_j
                complex_matrix phi_j = phi_j_.at(j);
                complex_matrix priorBeta_j = (phi_j.hadamard(currentVt)) * w_;
                priorBeta_j(1) *= 0.5;

                // Put column into B-hat
                for (unsigned short i = 1; i != N_ + 1; ++i)
                {
                    priorB_hat(i, j) = priorBeta_j(i);
                }
            }

            // Recover column vectors of C-hat using inverse-FFT
            complex_matrix fftInput = matrixM * priorB_hat;
            matrix priorContCoeff = exp(-r_*tau_)*(matrixM*priorB_hat).real();

            // Determine the prior coeff matrix V(t_m-1) and record
            matrix priorVt(N_, J_);
            if ((otype_ == 'c' && x0 <= xStar) || (otype_ == 'p' && x0 > xStar))
            {
                priorVt = priorContCoeff;
            }
            else // (call & x0 >= x*) or (put x <= x*)
            {
                priorVt = GetGMatrix(l, u);
            }
            currentVt = priorVt;
        }

        ContinuationFunction contF(r_, opt.GetExerciseInterval(),
                              a_,  b_, p, w_, currentVt, phi_j_);
        std::complex<double> sumBetaExp(0.0, 0.0);
        for (unsigned long n = 0; n != N_; ++n)
        {
            double dn = (double) n;
            std::complex<double> expFourier = std::exp(i*dn*PI*(x0-a_)/(b_-a_));
            std::complex<double> term = contF.beta(n) * expFourier;
            if (n == 0)   term *= 0.5;
            sumBetaExp += term;
        }

        double contVal = std::exp(-r_*tau_)*std::real(sumBetaExp);
        contValM[p] = contVal;
        
    }
    
    // Create spline interpolator with linear boundary derivative estimates
    double yp1Approx 
           = (contValM[2] - contValM[1])/(quadNodes_(2) - quadNodes_(1));
    double ypnApprox 
           = (contValM[J_] - contValM[J_-1])/(quadNodes_(J_) -quadNodes_(J_-1));
    matrix nodesRowVec
           = quadNodes_.transpose();
    std::vector<std::vector<double>> nodesVec 
           = (std::vector<std::vector<double>> ) nodesRowVec ;
    SplineInterpolation splineInterpolator( nodesVec[0], contValM, yp1Approx, 
                                            ypnApprox);

    optionValue_ = splineInterpolator.interp(std::log(v0_));
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	DetermineTruncationRanges()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void FangOosterlee::DetermineTruncationRanges(const ProcessBase & proc)
{
    // Setup newtons method
    LogVarPDF pdf(lambda_, T_, v0_, eta_, vbar_);
    NewtonMethod rootFinder(pdf, TOL);

    // Get initial guesses - corresponds with eq 11 of Fang & Oosterlee (2011)
    // common variables
    double exp_lambdaT = std::exp(-lambda_*T_);
    double expectedVar = v0_*exp_lambdaT + vbar_*(1.0 - exp_lambdaT);
    double logExpectedVar = std::log(expectedVar);
    double q = 2.0*lambda_*vbar_/etaSqr_ - 1.0;
    double aGuess = logExpectedVar - 5.0/std::sqrt(1.0 + q);
    double bGuess = logExpectedVar + 2.0/std::sqrt(1.0 + q);

    // Find roots
    av_ = rootFinder.Run(aGuess);
    bv_ = rootFinder.Run(bGuess);

    // Find a and b
    double mean = mu_*T_ + (1.0 - exp(-lambda_*T_))*(vbar_-v0_)/(2*lambda_)
                  - vbar_*T_/2.0;
    double std  = std::sqrt(vbar_*(1+eta_)*T_);
    double x0   = std::log(s0_/X_);
    double L    = 12.0;
    a_ = mean + x0 - L*std;
    b_ = mean + x0 + L*std;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	SetupQuadrature()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void FangOosterlee::SetupQuadrature()
{
    double h = (bv_ - av_)/(J_ - 1);
    double weight = 1;
    for (unsigned long j = 0; j != J_; ++j)
    {
        quadNodes_(j+1) =  av_ + h*j;
        w_(j+1) = weight;
        if (j == 0 || j == J_ - 1)   w_(j+1) = 0.5 * w_(j+1);
    }
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	ComputeVtM()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const matrix FangOosterlee::ComputeVtM()
{
    double l, u;
    switch(otype_)
    {
        case 'c':  l = 0.0; u = b_;      break;
        case 'p':  l = a_; u = 0.0;      break;
        default:   throw std::runtime_error("FangOosterlee:  Bad option type");
    }

    // Populate matrix V(t_M)
    matrix vt_M(N_, J_);
    PayoffCOSCoeff G(otype_, X_, a_, b_, l, u);
    for (unsigned long k = 0; k < N_; ++k)
    {
        // Compute row k
        double Gk = G(k);
        for (unsigned long j = 1; j <= J_; ++j)
        {
            vt_M(k+1, j) = Gk;
        }
    }

    return vt_M;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	ComputePhi()
//  This method correspond to the equation 29 of Fang and Oosterlee 2011
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const complex_matrix FangOosterlee::ComputePhi(unsigned long j)
{
    //define x_s
    double x_s = 0;

    //find zeta_j, exp(zeta_j)
    double zeta_j = quadNodes_(j);
    double exp_zeta_j = std::exp(zeta_j);

    // Constructor a Log-variance pdf functor
    LogVarPDF logVarPDF(lambda_, T_, v0_, eta_, vbar_);

    //define matrix
    complex_matrix phi_squiggle_j(N_, J_);

    //find matrix
    for(unsigned long p = 1; p != J_ + 1; ++p)
    {
        double zeta_p = quadNodes_(p);
        double exp_zeta_p = std::exp(zeta_p);
        LogVarConditionalChF ChF(lambda_, eta_, mu_, rho_, vbar_, 
                                 x_s, tau_, exp_zeta_j, exp_zeta_p);
        logVarPDF.SetTimes(tau_, zeta_p);
        double zetaJProbDensity = logVarPDF(zeta_j);

        for(unsigned long n = 0; n < N_; ++n)
        {
            double w = n*PI / (b_ - a_);
            std::complex<double> ChFw = ChF(w);
            std::complex<double> node_np = zetaJProbDensity*ChFw;
            phi_squiggle_j(n+1, p) = node_np;
        }
    }

    return phi_squiggle_j;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	DetermineEarlyExercise()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

double FangOosterlee::DetermineEarlyExercise(const OptionBermudanBase & opt,
                                             unsigned long p, const matrix & vt)
{
    // Instantiate continuation function and get memory managed pointer to it
    const ContinuationFunction * contFPtr
        = new ContinuationFunction (r_, opt.GetExerciseInterval(),
                                    a_,  b_, p, w_, vt, phi_j_);
    const OptionBermudanBase * optPtr = & opt;

    // Let eeValue(y) := contF(y) - payoff(y)
    LinearComposition<double, double>::pairCoeffFunc oneContF(1.0, contFPtr);
    LinearComposition<double, double>::pairCoeffFunc minusPayoff(-1.0, optPtr);
    LinearComposition<double, double>::vecPairCoeffFunc eeValueTerms
            = {oneContF, minusPayoff};
    LinearComposition<double, double> eeValue(eeValueTerms);

    // Find EE point - where eeValue(y) == 0
    NewtonMethod newtonMethod(eeValue, TOL);
    double initialGuess = 0.0;
    double EEpoint = newtonMethod.Run(initialGuess);

    // Clean up
    delete contFPtr;

    // Return early exercise point
    return EEpoint;
    
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	GetMcMatrix()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const complex_matrix FangOosterlee::GetMcMatrix(double l, double u)
{
    // Initialise matrix
    complex_matrix mc_matrix(N_, N_);

    // Initialise subscript functor
    M_J m_j(l, u, b_, a_);

    for(unsigned long c = 0; c != N_; ++c)
    {
        for(unsigned long d = 0; d != N_; ++d)
        {
            mc_matrix(c+1, d+1) = m_j(c + d);
        }
    }

    return mc_matrix;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	GetMsMatrix()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const complex_matrix FangOosterlee::GetMsMatrix(double l, double u)
{
    // Initialise matrix
    complex_matrix ms_matrix(N_, N_);

    // Initialise subscript functor
    M_J m_j(l, u, b_, a_);

    for(unsigned long c = 0; c != N_; ++c)
    {
        for(unsigned long d = 0; d != N_; ++d)
        {
            ms_matrix(c+1, d+1) = m_j(d - c);
        }
    }

    return ms_matrix;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	GetGMatrix()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const matrix FangOosterlee::GetGMatrix(double l, double u)
{
    // declare matrix GMatrix
    matrix GMatrix(N_, J_);
    PayoffCOSCoeff Gfunction(otype_, X_, a_, b_, l, u);
    for (unsigned long k = 0; k < N_; ++k)
    {
        // Compute an element row k
        double Gvalue = Gfunction(k);
        for (unsigned long j = 1; j <= J_; ++j)
        {
            GMatrix(k+1, j) = Gvalue;
        }
    }

    return GMatrix;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	RegisterOutput()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void FangOosterlee::RegisterOutput(Output & out)
{
    out.RegisterOutput("Option value: ", optionValue_);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
