//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//XX  Recursion_FFT.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "utility.h"
#include "Recursion_FFT.h"

#include <cmath>
#include <stdexcept>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	set PI_Sqr value
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

namespace
{
    const double PI_2 = 6.28318530717959;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	constructor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

complex_matrix FFT::ComputeFFT(const complex_matrix & input, bool forward)
{
    // Output matrix is of the same dimensions as the input
    long N = input.rows();
    long M = input.cols();
    complex_matrix output(N, M);

    // The transformation direction dictates the sign
    int isign = forward ? 1 : -1;

    // For each input column
    for (long i = 0; i != M; ++i)
    {
        // Fill {even, 0} elements with real parts and odd with complex parts
        double * colData = new double(2*N);
        for (long j = 0; j != N ; ++j)
        {
            colData[2*j]        = std::real(input(j+1,i+1));
            colData[2*j + 1]    = std::imag(input(j+1,i+1));
        }

        // Apply inverse FFT
        Recursion_FFT(colData, N, isign);

        // Store result in output matrix
        for (long j = 0; j != N; ++j)
        {
            output(j+1, i+i) = 
                        std::complex<double>(colData[2*j], colData[2*j + 1]);
        }
    }

    return output;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//XX  Recursion_FFT(). n is number of X's.  2n is real + complex parts  
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void FFT::Recursion_FFT(double * data, const long n, const int isign)
{
    if(1 == n) return;
        
    if(n&(n-1)) 
        throw(std::runtime_error("Recursion_FFT():  n must be a power of 2"));

    const long m = n/2;
       
    double * data_e = new double[n];       // for the even and odd parts
    double * data_o = new double[n];
    
    for(int k = 0; k != m; ++k)            // fill even and odd parts
    {
        int k2 = 2*k;        
        int k4 = 4*k;        
        data_e[k2]     = data[k4];         // even pairs in data
        data_e[k2 + 1] = data[k4 + 1];
        
        data_o[k2]     = data[k4 + 2];     // odd pairs in data
        data_o[k2 + 1] = data[k4 + 3];
    }

    Recursion_FFT(data_e, m, isign);       // call recursively on each part
    Recursion_FFT(data_o, m, isign);

	double theta = isign*PI_2/n;           // Note:  toggled by isign
	double wtemp = std::sin(0.5*theta);
	
	double wr1 = -2.0*wtemp*wtemp;         // w_1 - 1     
	double wi1 = std::sin(theta); 
         
	double wr = 1.0;                       // w_0
	double wi = 0.0;                   

    for(int k = 0; k != m; ++k)
    {
        long k1 = k % m;
        double t1 = wr*data_o[2*k1] - wi*data_o[2*k1 + 1];
        double t2 = wi*data_o[2*k1] + wr*data_o[2*k1 + 1];
        
        data[2*k]     = data_e[2*k1]     + t1;
        data[2*k + 1] = data_e[2*k1 + 1] + t2;  
              
        data[2*m + 2*k]     = data_e[2*k1]     - t1;
        data[2*m + 2*k + 1] = data_e[2*k1 + 1] - t2;      
          
		double wsafe = wr;
		wr += wr1*wsafe - wi1*wi;          // rolls over to k + 1
		wi += wr1*wi + wi1*wsafe;   
    }
    
    delete[] data_e;    
    delete[] data_o;

	if(-1 == isign) for (int i = 0; i != 2*n; ++i) data[i] /= 2;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//XX  end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
