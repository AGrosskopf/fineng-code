//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Input.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "Input.h"

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	interface
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Input::Input()
{
    S_0_ = 12;
    r_ = 0.1;
    sig_ = 0.20;
    mu_ = 0.1;          // r - delta
   
    v0_ = 0.0625;
                        // Set 1,  Set 2                                                                                                                  
    lambda_ = 5;        // 0.2,    0.4
    vbar_ = 0.16;       // 0.05,   0.04
    eta_ = 0.9;         // 0.1,    0.4
    
    rho_ = 0.1;
    
    X_ = 10;
    T_ = 0.25;
    Ex_int_ = 0.025;    // interval between possible exercise times

    O_type_ = 'p';   // c for call,  p  for put, a for average rate, r for ratchet
    P_type_ = 'h';   // g for gbm, h for Heston
    
    J_ = 16;
    N_ = 256;

    Acc_type_ = 'p';   // p for plain accumulator 
    Meth_type_ = 'f';  // f for Fang and Oosterlee
    v_type_ = 'm';     // m for MC

    // Unused
    CIR_type_ = 'c'; // c for approximate,  e for exact
    dX_ = 2;
    dB_ = 2;
    B0_ = 100;
    dT_ = 1.0/12;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	end
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
