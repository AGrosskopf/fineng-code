//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	LinearComposition.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include <string>
#include <stdexcept>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	constructor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

template <typename D, typename R>
LinearComposition<D,R>::LinearComposition(const vecPairCoeffFunc & terms)
: terms_(terms)
{
    // nothing else to do
}

template <typename D, typename R>
R LinearComposition<D,R>::operator()(const D x) const
{
    // Conventionally f(0, x) == f(x) for all (c,f) in terms_
    return this->operator()(0, x);
}

template <typename D, typename R>
R LinearComposition<D,R>::operator()(const unsigned long n, const D x) const
{
    // Stores the sum of the linear composition
    R sum = 0;

    // For each term
    for (pairCoeffFunc coeffFunc : terms_)
    {
        // Extract coefficient and function
        R c = coeffFunc.first;
        ptrDFB f = coeffFunc.second;

        // Add to sum
        sum += c * f->operator()(n , x);
    }

    // Return the sum
    return sum;
}
 
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
