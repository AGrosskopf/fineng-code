//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	PseudoFactory.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "PseudoFactory.h"
#include "Input.h"
#include "OptionBase.h"
#include "OptionBermudanCall.h"
#include "OptionBermudanPut.h"
#include "ProcessBase.h"
#include "ProcessHeston.h"
#include "MethodBase.h"
#include "FangOosterlee.h"
#include "ApplicationBase.h"
#include "ValuationApplication.h"

#include <stdexcept>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Getter
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

double PseudoFactory::GetS0() const {return inp_->GetS0();}
double PseudoFactory::GetR() const {return inp_->GetR();}
double PseudoFactory::GetSig() const {return inp_->GetSig();}
double PseudoFactory::GetMu() const {return inp_->GetMu();}

double PseudoFactory::GetV0() const {return inp_->GetV0();}
double PseudoFactory::GetLambda() const {return inp_->GetLambda();}
double PseudoFactory::GetVBar() const {return inp_->GetVBar();}
double PseudoFactory::GetEta() const {return inp_->GetEta();}
double PseudoFactory::GetRho() const {return inp_->GetRho();}
        
char PseudoFactory::GetPtype() const {return inp_->GetPtype();}

double PseudoFactory::GetX() const {return inp_->GetX();}
double PseudoFactory::GetT() const {return inp_->GetT();}

double PseudoFactory::GetExerciseInterval() const {return inp_->GetExerciseInterval();}

double PseudoFactory::GetdX() const {return inp_->GetdX();}
double PseudoFactory::GetdB() const {return inp_->GetdB();}
double PseudoFactory::GetB() const {return inp_->GetB();}
double PseudoFactory::GetdT() const {return inp_->GetdT();}

char PseudoFactory::GetOtype() const {return inp_->GetOtype();}

unsigned long PseudoFactory::GetJ() const {return inp_->GetJ();}
unsigned long PseudoFactory::GetN() const {return inp_->GetN();}
  
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	CreateOption
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

OptionBase * PseudoFactory::CreateOption()
{
    char o_type = inp_->GetOtype();
    
    switch(o_type)
    {
        case 'c':  return new OptionBermudanCall(*this);   	break;
        case 'p':  return new OptionBermudanPut(*this);   	break;
        default:   throw std::runtime_error("PseudoFactory::CreateOption:  Bad character");
    }
}
 
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	CreateProcess
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

ProcessBase * PseudoFactory::CreateProcess()
{
    char p_type = inp_->GetPtype();
    
    switch(p_type)
    {
        case 'h':  return new ProcessHeston(*this);         break;
        default:   throw std::runtime_error("PseudoFactory::CreateProcess:  Bad character");
    }
}
 
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	CreateMethod
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

MethodBase * PseudoFactory::CreateMethod()
{
    char meth_type = inp_->GetMethodType();

    switch(meth_type)
    {
        case 'f':  return new FangOosterlee(*this);		    break;
        default:   throw std::runtime_error("PseudoFactory::CreateMethod:  Bad character");
    }
}
 
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	CreateValuation
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

ApplicationBase * PseudoFactory::CreateApplication()
{
    char v_type = inp_->GetValuationtype();
    
    switch(v_type)
    {
        case 'm':  return new ValuationApplication(*this);         break;
        default:   throw std::runtime_error("PseudoFactory::CreateApplication:  Bad character");
    }
}
 
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	end
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
