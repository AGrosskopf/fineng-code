//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	BaseInterp.cpp
//  Code from Numerical Recipes Third Edition 
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "BaseInterp.h"

#include <cmath>
#include <algorithm>
#include <stdexcept>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	constructor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

BaseInterp::BaseInterp(const BaseInterp::vecDouble & x, 
                       const BaseInterp::vecDouble & y, BaseInterp::uLong m)
: xx_(x), yy_(y), n_(x.size()), m_(m), jsav_(0), cor_(0)
{
    dj_ = std::max(1, (int) pow((double) n_, 0.25));
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	locate()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

BaseInterp::uLong BaseInterp::locate(const double x)
{
    uLong ju = n_ - 1;   // upper limit of indices
    uLong jl = 0;        // lower limit of indices

    // Check # of global & local interpolation points > 1, local < global
    if (n_ < 2 || m_ < 2 || m_ > n_)
    {
        throw std::invalid_argument("locate size error");
    }

    //true if ascending order of table
    bool ascend = (xx_.at(n_ - 1) >= xx_.at(0)); 
    while (ju - jl > 1)
    {
        uLong jm = (ju + jl) >> 1;  // because dividing by 2 is so 2015
        if ((x >= xx_.at(jm)) == ascend)
        {
            jl = jm;
        }
        else
        {
            ju = jm;
        }
    }
    
    //decide whether to use hunt or locate next time
    cor_ = std::abs(jl - jsav_) > dj_ ? 0 : 1;
    
    jsav_ = jl;

    long bisectedIndex = std::min(n_ - m_, jl - ((m_ - 2) >> 1));
    uLong locatedIndex = (uLong) std::max(0L, bisectedIndex);
    return locatedIndex;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	hunt()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

unsigned long BaseInterp::hunt(const double x)
{
    uLong jl = jsav_;
    uLong inc = 1;

    uLong jm;
    uLong ju;

    // Check # of global & local interpolation points > 1, local < global
    if (n_ < 2 || m_ < 2 || m_ > n_)
    {
        throw std::invalid_argument("locate size error");
    }

    bool ascend = ((xx_.at(n_ - 1)) >= xx_.at(0));
    if (jl < 0 || jl > n_ - 1)
    {
        jl = 0;
        ju = n_ - 1;
    }
    else
    {
        if ((x >= xx_.at(jl)) == ascend) //hunt up
        {
            for (;;) // stopping condition at break statements
            {
                ju = jl + inc;
                if (ju >= n_ - 1)
                {
                    ju = n_ - 1;
                    break;
                }
                else if ((x < xx_.at(ju)) == ascend)
                {
                    break;
                }
                else
                {
                    jl = ju;
                    inc += inc;
                }
            }
        }
        else //hunt down
        {
            ju = jl;
            for (;;) // stopping condition at break statements
            {
                jl = jl - inc;
                if (jl <= 0)
                {
                    jl = 0;
                    break;
                }
                else if ((x >= xx_.at(jl)) == ascend)
                {
                    break;
                }
                else
                {
                    ju = jl;
                    inc += inc;
                }
            }
        }
    }
    
    //hunt is done, so begin the final bisection phase
    while (ju - jl > 1) 
    {
        jm = (ju + jl) >> 1;
        if ((x >= xx_.at(jm)) == ascend)
        {
            jl = jm;
        }
        else
        {
            ju = jm;
        }
    }
    
    cor_ = std::abs(jl - jsav_) > dj_ ? 0 : 1;
    jsav_ = jl;

    // Return hunted index
    long bisectedIndex = std::min(n_ - m_, jl - ((m_ - 2) >> 1));
    uLong locatedIndex = (uLong) std::max(0L, bisectedIndex);
    return locatedIndex;
}
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
