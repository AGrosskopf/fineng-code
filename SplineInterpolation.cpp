//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	SplineInterpolation.cpp
//  Code from Numerical Recipes Third Edition 
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "SplineInterpolation.h"

#include <cmath>
#include <algorithm>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	set the tolerance
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

namespace
{
    const double TOL = 0.99e99;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	constructor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

SplineInterpolation::SplineInterpolation(const doubleVec & xv, 
                                         const doubleVec & yv, 
                                         double yp1, double ypn)
:  BaseInterp(xv, yv,2), xv_(xv), yv_(yv), yp1_(yp1), ypn_(ypn), y2_(xv.size())
{
    sety2(xv, yv, yp1, ypn);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	sety2()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void SplineInterpolation::sety2(const doubleVec & xv, const doubleVec & yv, 
                                double yp1, double ypn)
{
    double p, qn, sig, un;
    unsigned long n = y2_.size();
    doubleVec u(n - 1);
    
    if (yp1 > TOL)
    {
        y2_.at(0) = u.at(0) = 0.0;
    }
    else
    {
        y2_.at(0) = -0.5;
        u.at(0) = (3.0/(xv_.at(1) - xv_.at(0)))
                  * ((yv_.at(1) - yv_.at(0))/(xv_.at(1) - xv_.at(0)) - yp1);
    }
    
    for(uLong i = 1; i < n - 1; ++i)
    {
        sig = (xv_.at(i) - xv_.at(i-1)) / (xv_.at(i+1) - xv_.at(i-1));
        p = sig * y2_.at(i-1) + 2.0;
        y2_.at(i) = (sig - 1.0) / p;
        u.at(i) = (yv_.at(i+1) - yv_.at(i)) / (xv_.at(i+1) - xv_.at(i)) 
                  - ((yv_.at(i) - yv_.at(i-1)) / (xv_.at(i) - xv_.at(i-1)));
        u.at(i) = (6.0 * u.at(i) / (xv_.at(i+1) - xv_.at(i-1)) 
                  - (sig * u.at(i-1))) / p;  
    }
    
    if (ypn > TOL)
    {
        qn = un = 0.0;
    }
    else
    {
        qn = 0.5;
        un = (3.0 / (xv_.at(n-1) - xv_.at(n-2))) 
             *(ypn - (yv_.at(n-1) - yv_.at(n-2)) / (xv_.at(n-1) - xv_.at(n-2)));
    }
    
    y2_.at(n-1) = (un - qn * u.at(n-2)) / (qn * y2_.at(n-2) + 1.0);
    
    for(long k = n - 2; k >= 0; --k)
    {
        y2_.at(k) = y2_.at(k) * y2_.at(k+1) + u.at(k);
    }
    
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	rawinterp()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

/*Given a value x and using pointers to data xx and yy, 
  and the stored vector of second derivatives y2, this 
  routine return the cubic spline interpolation value y*/ 

double SplineInterpolation::rawinterp(uLong jl, double x)
{
    uLong klo = jl;
    uLong khi = jl + 1;

    double h = xx_.at(khi) - xx_.at(klo);
    
    if (h == 0.0)
    {
        throw("Bad input to routine splint"); 
    }
    
    double a = (xx_.at(khi) - x) / h;
    double b = (x - xx_.at(klo)) / h;
    double y = a * yy_.at(klo) + b * yy_.at(khi) + ((a*a*a - a) * y2_.at(klo)
               + (b*b*b - b) * y2_.at(khi)) * (h*h) / 6.0;
    return y;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
