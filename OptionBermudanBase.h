//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  OptionBermudanBase.h
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#ifndef BermudanBaseH
#define BermudanBaseH
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "OptionBase.h"

#include <vector>

class PseudoFactory;

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  class OptionBermudanBase
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

class OptionBermudanBase : public OptionBase
{
    public:
        OptionBermudanBase(){}
        virtual ~OptionBermudanBase(){}
        OptionBermudanBase(const PseudoFactory & fact);
        virtual double GetT() const {return T_;};
        virtual double GetX() const {return X_;};
        virtual double GetExerciseInterval() const {return Ex_int_;}
        virtual long GetEarlyExerciseCount() const {return long(T_/N_ex_);};
        virtual bool IsEarlyExercise() const {return early_;}
        virtual bool IsExerciseTime(double t) const;
        
        // "Function" in the sense of its payoff function
        virtual double operator()(const double S) const = 0;
        virtual double operator()(const unsigned long, const double) const = 0;
        
    private:
        double X_;
        double T_;            // maturity time
        double Ex_int_;       // interval between possible exercise times.
        double N_ex_; 

		bool early_;

		double fuzz_;         // allowed error in rounding of double to long;
        
};

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#endif
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
