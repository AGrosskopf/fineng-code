//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	LogVarConditionalChF.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "LogVarConditionalChF.h"
#include "LogVarChF.h"

#include <iostream>
#include <cmath>
#include <stdexcept>
#include <complex>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	constructor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

LogVarConditionalChF::LogVarConditionalChF(double lambda, double eta, double mu,
                                           double rho, double vbar, double x_s,
                                           double tau, 
                                           double sigma_t, double sigma_s)
: lambda_(lambda), eta_(eta),
  mu_(mu), rho_(rho),
  vbar_(vbar) , x_s_(x_s), tau_(tau), 
  sigma_t_(sigma_t), sigma_s_(sigma_s)
{
    // Check validity of time parameters
    if (tau <= 0)
    {
        throw 
        std::runtime_error("LogVarConditionalChF::Constructor: Invalid times");
    }
    
    //Set sensible value
    SetTimes(x_s_, tau_, sigma_t_, sigma_s_);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	SetTimes()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void LogVarConditionalChF::SetTimes(double x_s, 
                                    double tau, double sigma_t, double sigma_s)
{
    if (tau <= 0)
    {
        throw 
        std::runtime_error("LogVarConditionalChF::SetTimes:  Invalid times");
    }
    
    // Update time-dependent variables
    x_s_ = x_s;
    tau_ = tau;
    sigma_s_ = sigma_s;
    sigma_t_ = sigma_t;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	LogVarConditionalChF::operator()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

std::complex<double> LogVarConditionalChF::operator()(double w) const
{
    //define a complex i
    std::complex<double> i(0.0, 1.0);
    
    // Compute newly determined common expressions
    double exp_t = std::exp(sigma_t_);
    double exp_s = std::exp(sigma_s_);
    double frac_rho = rho_*(exp_t - exp_s - lambda_*vbar_*tau_)/eta_;
    
    std::complex<double> exp_iw = std::exp(i*w*(x_s_ + mu_*tau_ + frac_rho));
    
    std::complex<double> omega = w*((lambda_*rho_)/eta_ - 0.5) 
                                 + 0.5*i*w*w*(1 - rho_*rho_);
    
    LogVarChF ChFW(lambda_, vbar_, eta_, tau_, exp_t, exp_s);

    std::complex<double> ChFCon = exp_iw * ChFW(omega); 
    
    return ChFCon;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
