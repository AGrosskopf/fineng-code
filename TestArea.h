//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  TestArea.h
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#ifndef TestAreaH
#define TestAreaH
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

class PseudoFactory;

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  namespace TestArea
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

namespace TestArea
{
    // Calls all tests
    void RunAllTests(const PseudoFactory & params);
    
	// Test subclasses of RealFunction
    void TestLogVarPDF(const PseudoFactory & params);
    void TestLogVarPDFPrime(const PseudoFactory & params);
    void TestLinearPoly();
    void TestModBesselIv();

    // Test NewtonsMethod class
    void TestNewtonMethod(const PseudoFactory & params);
    
    //Test SplineInterpolation
    void TestSpline();
    
    //Test Cumulants
    void TestCumulants(const PseudoFactory & params);

    // Test matrix stuff
    void TestMatrixMult();
    void TestComplexMatrixMult();
    void TestMixedMatrixMult();
}

namespace testing = TestArea;    //alias

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#endif
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  End
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
