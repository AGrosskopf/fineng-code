//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//	Group C: 1569975, 1609679, 1607990
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   complex_matrix.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "complex_matrix.h"
#include "matrix.h"
#include "utility.h"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <stdexcept>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  namespace std
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

namespace std
{
	template<> void swap(complex_matrix & q1, complex_matrix & q2)
	{
		q1.swap(q2);
	}
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  Structural stuff N_ and M_ are 1-based
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

// default constructor
complex_matrix::complex_matrix()                                        
: x_(0), N_(0), M_(0), S_(0)
{}

// NxM constructor,  m(i,j) = 0
complex_matrix::complex_matrix(long N, long M)                          
: x_(0), N_(N), M_(M), S_(N_*M_)
{
	check_size();
    x_ = new std::complex<double>[S_];
	for(long i = 0; i != S_; ++i) x_[i] = 0.0;
}

// NxM constructor,  m(i,j) = a 
complex_matrix::complex_matrix(long N, long M, std::complex<double> a)             
: x_(0), N_(N), M_(M), S_(N_*M_)
{
	check_size();
    x_ = new std::complex<double>[S_];
	for(long i = 0; i != S_; ++i) x_[i] = a;
} 

// Nx1 from a std::vector
complex_matrix::complex_matrix(const std::vector<std::complex<double>> & v)      
: x_(0)
{
    N_ = v.size();
    M_ = 1;
    S_ = N_;

    x_ = new std::complex<double>[S_];
	for(long i = 0; i != S_; ++i) x_[i] = v[i];
}

// NxM from a std::vector
complex_matrix::complex_matrix(long N, long M, 
                               const std::vector<std::complex<double>> & v) 
: x_(0), N_(N), M_(M), S_(N_*M_)
{
	check_size();
		
	if (S_ != long(v.size())) 
       throw std::runtime_error("complex_matrix: wrong size initializer");

    x_ = new std::complex<double>[S_];
    
    for(long i = 0; i != S_; ++i) x_[i] = v[i];  
}

// constructs from a v of vs
complex_matrix::complex_matrix(const std::vector<std::vector<std::complex<double>> > & v) 
: x_(0)
{
    N_ = v.size();
    M_ = v[0].size();
    S_ = N_*M_;
    
    x_ = new std::complex<double>[S_];    

	for(long i = 0; i != N_; ++i)
    {
        if(long(v[i].size()) != M_) 
        {
            delete [] x_;
            x_ = 0;
            throw std::runtime_error("complex_matrix: bad input vector");
        }
        for(long j = 0; j != M_; ++j) x_[i*M_ + j] = v[i][j];
    }
}

complex_matrix::complex_matrix(const matrix & realMatrix)
{
    N_ = realMatrix.rows();
    M_ = realMatrix.cols();
    S_ = N_*M_;

    x_ = new std::complex<double>[S_];

    for(long i = 0; i != N_; ++i)
        for(long j = 0; j != M_; ++j)
            x_[i*M_ + j] = realMatrix(i+1, j+1);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   member conversion operator
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

// constructs a v of vs from this
complex_matrix::operator std::vector<std::vector<std::complex<double>> >()  
{    
    std::vector<std::complex<double>> row(M_, 0.0);
    std::vector<std::vector<std::complex<double>> > R(N_, row);

	for(long i = 0; i != N_; ++i)
    {
        for(long j = 0; j != M_; ++j) R[i][j] = x_[i*M_ + j];
    }
    
    return R;
}

matrix complex_matrix::real() const
{
    matrix newMatrix(N_, M_);

    for(long i = 0; i != N_; ++i)
        for(long j = 0; j != M_; ++j)
        {
            double component = std::real(x_[i * M_ + j]);
            newMatrix(i + 1, j + 1) = component;
        }

    return newMatrix;
}


//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   rule of three
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

complex_matrix::~complex_matrix()
{
	delete [] x_;
}

complex_matrix::complex_matrix(const complex_matrix & original)
:	x_(0)	// Initialized to 0 so can delete harmlessly
{
	try
	{
		if (original.x_ != 0) x_ = original.clone_data();
	}
	catch(...)
	{
		delete [] x_;
		x_ = 0;
		throw;
	}

	N_ = original.N_;
	M_ = original.M_;
	S_ = original.S_;
}

complex_matrix & complex_matrix::operator=(const complex_matrix & original)
{
	complex_matrix temp(original);    // exception safe idiom
	swap(temp);          
	return *this;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  swap() and clone()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void complex_matrix::swap(complex_matrix & other)
{
	std::swap(x_, other.x_);		
	std::swap(N_, other.N_);		
	std::swap(M_, other.M_);		
	std::swap(S_, other.S_);		
}

complex_matrix * complex_matrix::clone() const
{
	return new complex_matrix(*this);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  minor()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

complex_matrix complex_matrix::minor(long I, long J) const  // 1-based
{
    check_indexes(I, J);
    
    long N = N_ - 1;
    long M = M_ - 1;
    
    complex_matrix m(N, M);
    
	for(long i = 0; i != N; ++i)
	{
        long ii = (i < I-1) ? i : i + 1;
        	    
	    for(long j = 0; j != M; ++j)
	    {
	        long jj = (j < J-1) ? j : j + 1;
            	    	    
	        m.x_[i*M + j] = x_[ii*M_ + jj];
    	}
	}
    return m;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   indexing.  1-based
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

// Indexing (r-value)
const std::complex<double> & complex_matrix::operator()(const long i, 
                                                        const long j) const 
{
    check_indexes(i, j);
    return x_[(i - 1)*M_ + j - 1];  
}

// Indexing (l-value)
std::complex<double> & complex_matrix::operator()(const long i, const long j)             
{
    check_indexes(i, j);
    return x_[(i - 1)*M_ + j - 1];  
}

// Nx1 case
const std::complex<double> & complex_matrix::operator()(const long i) const               
{
    return operator()(i, 1);  
}

std::complex<double> & complex_matrix::operator()(const long i)             
{
    return operator()(i, 1); 
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  Overload <<
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

std::ostream & operator<<(std::ostream & out, const complex_matrix & a)
{
    if(a.N_ == 0 || a.M_ == 0) return out;
    
	out << "( ";
	
	for(long i = 0; i != a.N_; ++i)
	{
	    out << a.x_[i*a.M_];
	    
	    for(long j = 1; j != a.M_; ++j)
	    {
    		out << ", " << a.x_[i*a.M_ + j]; 
    	}
	    if(i != a.N_ - 1) out << "  /  ";
	}
	
	return out << " )";
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   overloaded arithmetic operators                                                                              
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const complex_matrix operator+(const complex_matrix & m1, 
                               const complex_matrix & m2)
{
    complex_matrix temp(m1);
    temp += m2;
    return temp;    
}

const complex_matrix operator-(const complex_matrix & m1, 
                               const complex_matrix & m2)
{
    complex_matrix temp(m1);
    temp -= m2;
    return temp;    
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   complex_matrix multiplication                                                                             
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const complex_matrix operator*(const complex_matrix & m1, 
                               const complex_matrix & m2)
{
    complex_matrix temp(m1);
    temp *= m2;
    return temp;    
}
	
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   m1*m2-1                                                                          
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const complex_matrix operator/(const complex_matrix & m1, 
                               const complex_matrix & m2)
{
    complex_matrix temp(m1);
    temp /= m2;
    return temp;    
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   element wise arithmetic overloads                                                                         
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const complex_matrix operator+(const complex_matrix & m , 
                               const std::complex<double> & s)
{
    complex_matrix temp(m);
    temp += s;
    return temp;    
}
    
const complex_matrix operator-(const complex_matrix & m , 
                               const std::complex<double> & s)
{
    complex_matrix temp(m);
    temp -= s;
    return temp;    
}
    
const complex_matrix operator*(const complex_matrix & m , 
                               const std::complex<double> & s)
{
    complex_matrix temp(m);
    temp *= s;
    return temp;    
}
    
const complex_matrix operator/(const complex_matrix & m , 
                               const std::complex<double> & s)
{
    complex_matrix temp(m);
    temp /= s;
    return temp;    
}
    		
const complex_matrix operator+(const std::complex<double> & s, 
                               const complex_matrix & m){return m + s;}
const complex_matrix operator*(const std::complex<double> & s, 
                               const complex_matrix & m){return m * s;}

const complex_matrix complex_matrix::hadamard(const matrix & rhs)
{
    long rhs_N = rhs.rows();
    long rhs_M = rhs.cols();

    if (N_ != rhs_N || M_ != rhs_M)
        throw std::runtime_error("complex_matrix hadamard.: bounds mismatch");

    complex_matrix result(N_, M_);

    for(long i = 1; i != N_ + 1; ++i)
    {
        for(long j = 1; j != M_ + 1; ++j)
        {
            std::complex<double> elementMulti =this->operator()(i, j)*rhs(i, j);
            result(i, j) = elementMulti;
        }
    }

    return result;
}

const complex_matrix complex_matrix::hadamard(const complex_matrix & rhs)
{
    long rhs_N = rhs.rows();
    long rhs_M = rhs.cols();

    if (N_ != rhs_N || M_ != rhs_M)
        throw std::runtime_error("complex_matrix hadamard.: bounds mismatch");

    complex_matrix result(N_, M_);

    for(long i = 1; i != N_ + 1; ++i)
    {
        for(long j = 1; j != M_ + 1; ++j)
        {
            result(i, j) = this->operator()(i, j) * rhs(i, j);
        }
    }

    return result;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   arithmetic assignment                                                                          
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

complex_matrix & complex_matrix::operator+=(const complex_matrix & rhs)
{
	if ((N_ != rhs.N_) || (M_ != rhs.M_)) 
       throw std::runtime_error("complex_matrix: bounds mismatch");

	for(long i = 0; i != S_; ++i) x_[i] += rhs.x_[i]; // element wise
    return *this;
}

complex_matrix & complex_matrix::operator-=(const complex_matrix & rhs)
{
	if ((N_ != rhs.N_) || (M_ != rhs.M_)) 
       throw std::runtime_error("complex_matrix: bounds mismatch");

	for(long i = 0; i != S_; ++i) x_[i] -= rhs.x_[i]; // element wise
    return *this;
}

// complex_matrix multiplication
complex_matrix & complex_matrix::operator*=(const complex_matrix & rhs)  
{
    long m2_N = rhs.N_;
    long m2_M = rhs.M_;

	if (M_ != m2_N) 
       throw std::runtime_error("complex_matrix mult.: bounds mismatch");

	complex_matrix result(N_, m2_M);
	
	for(long i = 0; i != N_; ++i)
	{
	    for(long j = 0; j != m2_M; ++j) 
        {
            long S = i*m2_M + j;
            result.x_[S] = 0.0;
            
            for(long k = 0; k != m2_N; ++k)
            {                
                result.x_[S] += x_[i*M_ + k] * rhs.x_[k*m2_M + j];
            }
        }
    }
	(*this) = result;     
    return *this;
}

complex_matrix & complex_matrix::operator/=(const complex_matrix & rhs)
{
    (*this) *= rhs.inverse();  // multiplication by a complex_matrix inverse
    return *this;
}
		
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   element wise scalar ops                                                                           
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

complex_matrix & complex_matrix::operator+=(const std::complex<double> & rhs)
{
	for(long i = 0; i != S_; ++i) x_[i] += rhs;
    return *this;
}

complex_matrix & complex_matrix::operator-=(const std::complex<double> & rhs)
{
	for(long i = 0; i != S_; ++i) x_[i] -= rhs;
    return *this;
}

complex_matrix & complex_matrix::operator*=(const std::complex<double> & rhs)
{
	for(long i = 0; i != S_; ++i) x_[i] *= rhs;
    return *this;
}

complex_matrix & complex_matrix::operator/=(const std::complex<double> & rhs)
{
	for(long i = 0; i != S_; ++i) x_[i] /= rhs;
    return *this;
}
		
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   matix stuff                                                                            
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

std::complex<double> complex_matrix::trace() const
{
    long N = std::min(N_, M_);
    
    std::complex<double> tr = 0.0;
    for(long i = 0; i != N; ++i) tr += x_[i*N + i];
    
    return tr;
}

std::complex<double> complex_matrix::det() const
{
    if(N_ != M_) 
     throw std::runtime_error("complex_matrix::det: complex_matrix not square");
    
    if(1 == N_) return x_[0];
    
    if(2 == N_) return x_[0]*x_[3] - x_[1]*x_[2];
    
    if(3 == N_)  return x_[0]*(x_[4]*x_[8] - x_[5]*x_[7]) 
                          - x_[1]*(x_[3]*x_[8] - x_[5]*x_[6])
                          + x_[2]*(x_[3]*x_[7] - x_[4]*x_[6]);
               
   	std::complex<double> dt(0.0, 0.0);
   	std::complex<double> sgn(1.0, 0.0);
    for(long i = 0; i != N_; ++i)
   	{
   	    complex_matrix m = this->minor(1, i+1);
   	    dt += sgn*x_[i]*m.det();
   	    sgn = -sgn;
   	}
    return dt;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   transpose()                                                                           
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const complex_matrix complex_matrix::transpose() const
{
    complex_matrix m(M_, N_);

	for(long i = 0; i != M_; ++i)
	{	    
	    for(long j = 0; j != N_; ++j)
	    {          	    	    
	        m.x_[i*N_ + j] = x_[j*M_ + i];
    	}
	}	
    return m;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   inverse()                                                                           
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const complex_matrix complex_matrix::inverse() const
{
    if(N_ != M_) 
     throw std::runtime_error("complex_matrix::det: complex_matrix not square");
    
    std::complex<double> dt = det();
    complex_matrix m(N_, N_);
    if(2 == N_) 
    {
        m(1,1) = x_[3]/dt;   m(1,2) = -x_[1]/dt;
        m(2,1) = -x_[2]/dt;  m(2,2) = x_[0]/dt;
        return m;
    }
    
    std::complex<double> sgn_i(1.0, 0.0);
	for(long i = 0; i != N_; ++i)
	{	    
        std::complex<double> sgn_j(1.0, 0.0);
	    for(long j = 0; j != N_; ++j)
	    {          	    	    
	        m.x_[i*N_ + j] = sgn_i*sgn_j * minor(j+1, i+1).det()/dt;
	        sgn_j = -sgn_j;
    	}
    	sgn_i = -sgn_i;
	}
    return m;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   comparison operators                                                                            
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

const bool operator!=(const complex_matrix & m1, const complex_matrix & m2)
{
	if ((m1.N_ != m2.N_) || (m1.M_ != m2.M_)) return true;

	long S = m2.S_;
	for(long i = 0; i != S; ++i) 
    {
        if(m1.x_[i] != m2.x_[i]) return true;
    }
	
	return false;
}

const bool operator==(const complex_matrix & m1, const complex_matrix & m2)
{
    return !(m1 != m2);
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   helpers                                                                             
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

bool complex_matrix::check_indexes(long i, long j) const    // checks indexes
{
    if(i < 1 || i > N_) 
        throw std::runtime_error("complex_matrix: i out of bounds");
    if(j < 1 || j > M_) 
        throw std::runtime_error("complex_matrix: j out of bounds");
    
    return true;
}

bool complex_matrix::check_size() const    // checks array bounds
{
	if (N_ <= 0 || M_ <= 0) 
       throw std::runtime_error("complex_matrix::check_size: bad size");
    
    return true;
}

bool complex_matrix::same_size(long N, long M) const // checks array bounds
{
    if (N_ != N || M_ != M) 
       throw std::runtime_error("complex_matrix::same_size: bad size");
    
    return true;
}

bool complex_matrix::compatible_size(long N) const   // checks compatibility
{
    if (N != N_) 
       throw std::runtime_error("complex_matrix: cannot multiply");
    
    return true;
}

std::complex<double> * complex_matrix::clone_data() const
{
	std::complex<double> * new_x = new std::complex<double>[S_];
	for(long i = 0; i != S_; ++i) new_x[i] = x_[i];
	return new_x;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//   end of file                                                                              
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

