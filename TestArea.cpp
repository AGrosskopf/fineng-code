//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  TestArea.cpp
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

#include "LogVarPDF.h"
#include "LinearComposition.h"
#include "NewtonMethod.h"
#include "TestArea.h"
#include "ModBesselIv.h"
#include "ProcessHeston.h"

#include "SplineInterpolation.h"

#include <complex>
#include "matrix.h"
#include "complex_matrix.h"

#include <iostream>
#include <vector>

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  RunAllTests()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void TestArea::RunAllTests(const PseudoFactory & params)
{
    TestLinearPoly();
    TestLogVarPDF(params);
    TestNewtonMethod(params);
    TestModBesselIv();
    TestCumulants(params);
    TestSpline();
    TestMatrixMult();
    TestComplexMatrixMult();
    TestMixedMatrixMult();
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  TestSpline()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void TestArea::TestSpline()
{
    std::vector<double> xv({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
    std::vector<double> yv({0.841471, 0.909297, 0.14112, -0.7568, -0.95892,
                           -0.27942, 0.656987, 0.989358, 0.412118, -0.54402});
    
    double yp1 = 0.5403023;
    double ypn = -0.8390715291;
    
    SplineInterpolation test_spline( xv, yv, yp1, ypn);
    
    double x = 1.5;
    double result = test_spline.interp(x);
    
    std::cout << "Got: " << result << std::endl;

    std::cout << "Expecting: 0.9947" << std::endl;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  TestModBesselIv()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void TestArea::TestModBesselIv()
{
    double order = 0.0;
    ModBesselIv myBesseyFunction(order);
    double result;
    for (double x = 0.1; x <= 2; x += 0.1)
    {
        result = myBesseyFunction(x);
        std::cout << x << ": " << result << std::endl;
    }
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  TestLinearPoly()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void TestArea::TestLinearPoly()
{
//    // f(x) = 3 + 4x
//    LinearComposition<double, double> myFavPoly(3, 4);
//
//    // Check f(x) = -7 at x = -2.5
//    std::cout << "Expecting x = -7.0" << std::endl;
//    std::cout << "Computed x = " << myFavPoly(-2.5) << std::endl;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  TestLogVarPDF()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void TestArea::TestLogVarPDF(const PseudoFactory & params)
{
    // Input data
    double sigt = 0.5;

    // Test original function
    std::cout << "LogVarPDF: " << std::endl;
    LogVarPDF testPDF(5, 0.25, 0.0625, 0.9, 0.16);
    std::cout << "Got: " << testPDF(sigt) << std::endl;
    std::cout << "Expecting: 3.9829e-09" << std::endl << std::endl;

    // Test first derivative
    unsigned long derivative = 1;
    double result = testPDF(derivative, sigt);
    std::cout << "LogVarPDF (1st derivative): " << std::endl;
    std::cout << "Got: " << result << std::endl;
    std::cout << "Expecting: 1.0458e-07" << std::endl << std::endl;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  TestNewtonMethod()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void TestArea::TestNewtonMethod(const PseudoFactory & params)
{
    // f(x) = 7 - 3x, f'(x) = -3
//    LinearComposition f (7, -3);
//
//    // Run Newtons method
//    NewtonMethod testNewton(f, 0.000001);
//    double root = testNewton.Run(0.05);
//
//    // Check result is as expected
//    std::cout << "Expecting x = 2.333" << std::endl;
//    std::cout << "Computed x = " << root << std::endl;
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  TestCumulants()
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void TestArea::TestCumulants(const PseudoFactory & params)
{
    ProcessHeston testCumu(params);

    std::map<unsigned long, double> cumulants = testCumu.GetCumulants();
    
    std::cout << "C1 C++: " << cumulants[1] << std::endl;
    std::cout << "C1 expecting: 0.03"  << std::endl;
    
    std::cout << "C2 C++: " << cumulants[2] << std::endl;
    std::cout << "C2 expecting: 0.0357"  << std::endl;
    
}

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  Test multiplication for all combinations of matrix, complex_matrix
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void TestArea::TestMatrixMult()
{
    // Input
    matrix A({{1.0, 2.0, 3.0} , {4.0, 5.0, 6.0}, {7.0, 8.0, 9.0}});
    matrix B({{4.0, 5.0, 6.0}, {7.0, 8.0, 9.0}, {1.0, 2.0, 3.0}});

    // Expected
    matrix expected ({{21.0, 27.0, 33.0}, {57.0, 72.0, 87.0}, {93.0, 117.0, 141.0}});

    // Compute actual result
    matrix result = A * B;

    // Print results
    std::cout << "Matrix-Matrix multiplication test: ";
    bool pass = expected == result;
    if (pass)   std::cout << "PASS";
    else        std::cout << "FAIL";
    std::cout << std::endl;
}

void TestArea::TestComplexMatrixMult()
{
    // Input
    std::complex<double> i(0.0, 1.0);
    complex_matrix A({{1.0*i, 4.5 + 2.0*i, 3.0*i} , {4.0*i, 5.0*i, 6.0*i}, {7.0*i, 8.0*i, 9.0*i}});
    complex_matrix B({{4.0*i, 5.0*i, 6.0*i}, {7.0*i, 9.7 + 8.0*i, 9.0*i}, {1.0*i, 2.1 + 2.0*i, 3.0*i}});

    // Expected
    complex_matrix expected({{ -21.0 + 31.5*i, 16.65 + 61.7*i, -33.0 + 40.5*i},
                             { -57.0 + 0.0*i, -72.0 + 61.1*i, -87.0 + 0.0*i},
                             { -93.0 + 0.0*i, -117.0 + 96.5*i, -141.0 + 0.0*i}});

    // Compute actual result
    complex_matrix result = A * B;

    // Print results
    std::cout << "C_Matrix-C_Matrix multiplication test: ";
    bool pass = expected == result;
    if (pass)   std::cout << "PASS";
    else        std::cout << "FAIL";
    std::cout << std::endl;
}

void TestArea::TestMixedMatrixMult()
{
    // Input
    std::complex<double> i(0.0, 1.0);
    matrix A({{1.0, 2.0, 3.0} , {4.0, 5.0, 6.0}, {7.0, 8.0, 9.0}});
    complex_matrix B({{4.0*i, 5.0*i, 6.0*i}, {7.0*i, 9.7 + 8.0*i, 9.0*i}, {1.0*i, 2.1 + 2.0*i, 3.0*i}});

    // Expected
    complex_matrix expected({{0.0 + 21.0*i, 25.7 + 27.0*i,    0.0 + 33.0*i},
                             {0.0 + 57.0*i, 61.1 + 72.0*i,    0.0 + 87.0*i},
                             {0.0 + 93.0*i, 96.5 + 117.0*i,   0.0 + 141.0*i}});

    // Compute actual result
    complex_matrix result = A*B;

    // Print results
    std::cout << "Matrix-C_Matrix multiplication test: ";
    bool pass = expected == result;
    if (pass)   std::cout << "PASS";
    else        std::cout << "FAIL";
    std::cout << std::endl;
}


//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//  end of file
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

